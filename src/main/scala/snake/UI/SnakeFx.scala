package snake.UI

import javafx.concurrent.Task
import scalafx.Includes._
import scalafx.application.{JFXApp3, Platform}
import scalafx.scene.Scene
import scalafx.scene.paint.Color

import snake.Logic.Game


object SnakeFx extends JFXApp3 {
  private val game = new Game

  private val updateGameLoop = new Task[Unit] {
    override def call(): Unit = {
      while(true) {
        game.updateState()
        Platform.runLater {
          stage.scene().content = game.getShapes.map(_.getShape).flatten.toList
        }
        Thread.sleep(1000 / 25 * 2)
      }
    }
  }

  override def start(): Unit = {
    stage = new JFXApp3.PrimaryStage {
      title = "Scala Snake (OO)"
      width = 600
      height = 600
      scene = new Scene {
        fill = Color.Black
        content = game.getShapes.map(_.getShape).flatten.toList
        onKeyPressed = key => game.changeDirection(key.getCode.getChar.toLowerCase)
      }
    }

    new Thread(updateGameLoop).start()
  }
}