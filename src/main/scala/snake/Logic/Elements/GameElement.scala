package snake.Logic.Elements

import scalafx.scene.Node

abstract class GameElement {
  def getShape(): List[Node]
}
