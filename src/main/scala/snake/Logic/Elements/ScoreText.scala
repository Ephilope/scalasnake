package snake.Logic.Elements

import scalafx.scene.paint.Color
import scalafx.scene.text.Text

class ScoreText() extends GameElement{
  private var score: Int = 0
  def newScore(): Unit = {
    score += 1
  }
  def resetScore(): Unit = {
    score = 0
  }

  override def getShape: List[Text] = {
    val scoreText = new Text()
    scoreText.text = "Score: " + score.toString
    scoreText.x = 10
    scoreText.y = 20
    scoreText.fill = Color.White
    List(scoreText)
  }
}
