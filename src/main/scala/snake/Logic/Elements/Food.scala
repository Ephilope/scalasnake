package snake.Logic.Elements

import scalafx.scene.paint.Color
import scalafx.scene.shape.Circle

import scala.util.Random

class Food() extends GameElement {
  private var position: (Double, Double) = (45 + scala.util.Random.nextInt(34) * 15, 45 + scala.util.Random.nextInt(34) * 15)

  def generateRandomPosition(): Unit = {
    val randomX = 45 + Random.nextInt(34) * 15
    val randomY = 45 + Random.nextInt(34) * 15
    position = (randomX, randomY)
  }

  def getPosition: (Double, Double) = {
    position
  }


  override def getShape: List[Circle] = {
    val foodCircle = new Circle()
    foodCircle.centerX = position._1 + 7.5
    foodCircle.centerY = position._2 + 7.5
    foodCircle.radius = 7.5
    foodCircle.fill = Color.Red
    List(foodCircle)
  }

}