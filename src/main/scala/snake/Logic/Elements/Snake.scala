package snake.Logic.Elements

import scalafx.scene.Node
import scalafx.scene.paint.Color
import scalafx.scene.shape.Rectangle
class Snake(var food: Food, var score: ScoreText) extends GameElement {
  private var snake: List[(Double, Double)] = List((240, 200), (225, 200), (210, 200))
  private var direction: Int = 4 // right

  def move(): Unit = {
    val (x, y) = snake.head
    val (newx, newy) = direction match {
      case 1 => (x, y - 15)  // up
      case 2 => (x, y + 15)  // down
      case 3 => (x - 15, y)  // left
      case 4 => (x + 15, y)  // right
      case _ => (x, y)
    }
    //si la tête du serpent sort du cadre ou touche son corps
    if (newx < 0 || newx >= 600 || newy < 0 || newy >= 600 || snake.tail.contains((newx, newy))) {
      //on réinitialise le serpent et le score
      resetSnake()
      score.resetScore()

    } else {
      var foodpos=food.getPosition
      //si la tête du serpent est sur la nourriture
      if (Math.abs(newx - foodpos._1) <= 10 && Math.abs(newy - foodpos._2) <= 10) {
        snake = (newx, newy) :: snake // alors on ajoute une case au serpent
        food.generateRandomPosition() // on génère une nouvelle position pour la nourriture
        //on vérifie que la nourriture n'est pas dans le corps du serpent
        foodpos=food.getPosition
        if (isCoordinateInBody(foodpos._1,foodpos._2)) {
          food.generateRandomPosition()
        }
        score.newScore()
      } else {
        snake = (newx, newy) :: snake.init
      }
    }
  }

  def isCoordinateInBody(x: Double, y: Double): Boolean = {
    snake.contains((x, y))
  }

  private def resetSnake(): Unit = {
    snake = List((240, 200), (225, 200), (210, 200))
    direction = 4
  }

  def changeDirection(input: String): Unit = {
    direction = input match {
      case "z" => if (direction != 2) 1 else 2
      case "s" => if (direction != 1) 2 else 1
      case "q" => if (direction != 4) 3 else 4
      case "d" => if (direction != 3) 4 else 3
      case _ => direction
    }
  }


  override def getShape(): List[Node] = {
    snake.map {
      case (x, y) =>
        val rect = new Rectangle()
        rect.x = x
        rect.y = y
        rect.width = 15
        rect.height = 15
        rect.fill = Color.White
        rect
    }
  }

}
