package snake.Logic

import snake.Logic.Elements.{Food, GameElement, ScoreText, Snake}

import scala.util.Random

class Game {
  private var food: Food = new Food()
  private var score: ScoreText = new ScoreText()
  private var snake: Snake = new Snake(food, score)

  def randomFood(): (Double, Double) = {
    var newFood: (Double, Double) = (45 + Random.nextInt(34) * 15, 45 + Random.nextInt(34) * 15)

    if (snake.isCoordinateInBody(newFood._1, newFood._2)) {
      newFood = randomFood()
    }
    newFood
  }

  def changeDirection(input: String): Unit = {
    snake.changeDirection(input)
  }

  def getShapes: List[GameElement] = {
    val shapes = List[GameElement](snake, food, score)
    shapes
  }
  def updateState(): Unit = {
    snake.move()
  }

}

